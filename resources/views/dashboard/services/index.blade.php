@extends('layouts.dashboard')

@section('content')

    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ $page_title }}</h3>
                <div class="nk-block-des text-soft">
                    <p>You have total {{ $tenders->count() }} Tenders.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/services/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Create New Tender</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="nk-tb-list is-separate mb-3">
            <div class="nk-tb-item nk-tb-head">

                <div class="nk-tb-col"><span class="sub-text">Tender ID</span></div>
                <div class="nk-tb-col"><span class="sub-text">Description</span></div>
                <div class="nk-tb-col tb-col-md"><span class="sub-text">Closing Date & Time</span></div>
                <div class="nk-tb-col"><span class="sub-text">District</span></div>
                <div class="nk-tb-col"><span class="sub-text">Department</span></div>
                <div class="nk-tb-col"><span class="sub-text">--</span></div>
            </div><!-- .nk-tb-item -->
            @if(count($tenders ) > 0)
                @foreach($tenders as $tender)
                    <div class="nk-tb-item">


                        <div class="nk-tb-col">
                            <a href="{{ url('/dashboard/acts/'.$tender->id.'/edit') }}">
                                <span class="tb-lead">   {{ $tender->tender_no }} </span>
                            </a>
                        </div>

                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $tender->description }}</span>
                        </div>

                        <div class="nk-tb-col tb-col-lg">
                            <span> {{ $tender->last_date }}</span>
                        </div>



                    </div><!-- .nk-tb-item -->

                @endforeach
            @else

            @endif


        </div><!-- .nk-tb-list -->
        <div class="card">
            <div class="card-inner">
                <div class="nk-block-between-md g-3">
                    {{ $tenders->links() }}

                </div><!-- .nk-block-between -->
            </div><!-- .card-inner -->
        </div><!-- .card -->
    </div>

@endsection
