@extends('layouts.user-dashboard')

@section('content')


    <style type="text/css">
        .calendar {
            display: flex;
            position: relative;
            padding: 16px;
            margin: 0 auto;
            max-width: 320px;
            background: white;
            border-radius: 4px;
            box-shadow: 0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04);
        }

        .timesheet-header{

        }

        .month-year {
            position: absolute;
            bottom:62px;
            right: -27px;
            font-size: 2rem;
            line-height: 1;
            font-weight: 300;
            color: #94A3B8;
            transform: rotate(90deg);
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
        }

        .year {
            margin-left: 4px;
            color: #CBD5E1;
        }

        /*.days {*/
        /*    display: flex;*/
        /*    flex-wrap: wrap;*/
        /*    flex-grow: 1;*/
        /*    margin-right: 46px;*/
        /*}*/

        /*.day-label {*/
        /*    position: relative;*/
        /*    flex-basis: calc(14.286% - 2px);*/
        /*    margin: 1px 1px 12px 1px;*/
        /*    font-weight: 700;*/
        /*    font-size: 0.65rem;*/
        /*    text-transform: uppercase;*/
        /*    color: #1E293B;*/
        /*}*/

        /*.day {*/
        /*    position: relative;*/
        /*    flex-basis: calc(14.286% - 2px);*/
        /*    margin: 1px;*/
        /*    cursor: pointer;*/
        /*    font-weight: 300;*/
        /*}*/

        /*.day.dull {*/
        /*    color: #94A3B8;*/
        /*}*/

        /*.day.today {*/
        /*    color: #0EA5E9;*/
        /*    font-weight: 600;*/
        /*}*/

        /*.day::before {*/
        /*    content: '';*/
        /*    display: block;*/
        /*    padding-top: 100%;*/
        /*}*/

        /*.day:hover {*/
        /*    background: #E0F2FE;*/
        /*}*/

        /*.day .content {*/
        /*    position: absolute;*/
        /*    top: 0;*/
        /*    left: 0;*/
        /*    height: 100%;*/
        /*    width: 100%;*/
        /*    display: flex;*/
        /*    justify-content: center;*/
        /*    align-items: center;*/
        /*}*/
    </style>
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">


            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title"</h3>
                <div class="nk-block-des text-soft">
                    <p>Wednesday, 27 October.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/timesheet/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Add Entry </span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="row g-gs">
            {!! $html !!}


            <!-- Styled Table -->
            <div class="card card-bordered card-preview">

                <div class = "btn-group">
                    <button type="button" class="btn btn-outline-light">Mon </button>
                    <button type="button" class="btn btn-outline-light">Tue </button>
                    <button type="button" class="btn btn-outline-light">Wed </button>
                    <button type="button" class="btn btn-outline-light">Thur </button>
                    <button type="button" class="btn btn-outline-light">Fri </button>
                    <button type="button" class="btn btn-outline-light">Sat </button>
                    <button type="button" class="btn btn-outline-light">Sun </button>
                </div>

                <table class = "table" id = "table1">

                </table>
            </div>

            <!-- Styled Table -->


            <!-- Modal Form -->
            <div class="modal fade" tabindex="-1" id="modalForm">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">New entry for </h5>
                            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                <em class="icon ni ni-cross"></em>
                            </a>
                        </div>
                        <div class="modal-body">
                        {!! Form::model($timesheet, array( 'method' => $formMethod, 'data-parsley-validate','class'=>'horizontal-form','id' => 'form1','files' => 'true', 'url' => $url ,'enctype'=>'multipart/form-data')) !!}
                        <!-- <form class = "horizontal-form" id = "form1"> -->
                                <div class="form-group">
                                    <label class="form-label">Projects / Tasks</label>
                                    <div class="form-control-wrap">
                                        {!! Form::select('projectID',$modaldata->pluck('project_name','id'),null, ['data-placeholder' => 'Select a project','class' => 'form-control form-select','id' => 'projectOption', 'data-search'=>'on', 'required' =>'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="form-label">Category</label>
                                    <div class="form-control-wrap">
                                        {!! Form::select('category', [""=>"",'Programming'=>'Programming','Testing'=>'Testing'] ,null, ['data-placeholder' => 'Select Category','class' => 'form-control form-select','id' => 'category' , 'data-search'=>'on', 'required' =>'required']) !!}
                                    </div>
                                </div>

                                <div class = "row">

                                    <div class="col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control-wrap">
                                                <div class="form-icon form-icon-left">
                                                    <em class="icon ni ni-calendar"></em>
                                                </div>
                                                {!! Form::text('date',null, ['class' => 'form-control date-picker ','id' => 'selectDate' , 'placeholder'=>'select date','required' =>'required',' data-date-format'=>'yyyy-mm-dd']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control-wrap">
                                                <div class="form-icon form-icon-left">
                                                    <em class="icon ni ni-clock"></em>
                                                </div>
                                                {!! Form::text('time',null, ['class' => 'form-control  ', 'id' => 'selectDuration','placeholder'=>'Duration','required' =>'required']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="form-label" for="comments">Comments</label>
                                    <div class="form-control-wrap">
                                        {!! Form::textarea('comments',null, ['class' => 'form-control no-resize','rows' => 1,'id' => 'comments' ,'placeholder'=>'Notes (Optional)','required' =>'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="submit" class="btn btn-lg btn-primary" data-dissmiss="modal" id = "btn1" value = "Start Timer">
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalForm">Modal With Form</button>
        </div>
    </div>


@endsection


<!-- To be Deleted!! -->
<!-- Ajax / JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>



<script type="text/javascript">

    $(document).ready(function(){

        var status = null;
        var id = null;
        // update time
        var interval = 1000 * 60 * 1;

        timerFunction = function()
        {
            if(status != 'running'){
                return false;
            }
            else{
                $.ajax({
                    url: '/updateTimer',
                    method: 'get',
                    data:{'id':id},
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success:function(data){
                        if(data == 'success'){
                            loadTable();
                        }
                    }
                })
            }
        }

        // timesheet table dynamic loading
        loadTable();

        setInterval(timerFunction,interval);

        function updateStatusAndId(statusValue,idValue){
            status = statusValue;
            id = idValue;
        }

        function loadTable(){
            $.ajax({
                url: '/loadTable',
                method: 'get',
                data:{},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    $('#table1').html('');
                   $('#table1').append(data.htmlQuery);
                   updateStatusAndId(data.status,data.id);
                }
            })
        }

        //Form submission using ajax
        $('#form1').on('submit',function(e){
            e.preventDefault()
            var formData = $(this);
            var url = formData.attr('action');
            var projectName = $('#projectOption option:selected').text();
            formData = formData.serialize()
            $.ajax({
                url: url,
                method: 'post',
                data: formData,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(response){
                    loadTable();
                    $("#modalForm").modal('hide')
                }
            });
        });

        //binding edit button to content loaded from controller
        $(document).on('click','.editBtn',function(){
            id = $(this).closest('tr').attr('id');
            $.ajax({
                url: 'fetchTimeSheet',
                method: 'get',
                data:{'id' : id},
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    $('#projectOption').val(data[0].project_id).change();
                    $('#category').val(data[0].category).change();
                    $('#selectDate').val(data[0].date).change();
                    $('#selectDuration').val(data[0].total_hours).change();
                    $('#comments').val(data[0].comments).change();
                    // $('#category option[value='+ data[0].category +']').attr('selected','selected');
                }
            })
        })


        //binding start and stop button to content loaded from controller
        $(document).on('click','.startstopBtn',function(){
            timesheetID = $(this).closest('tr').attr('id');

            $.ajax({
                url:'/timerStartStop',
                method:'get',
                data:{'id':timesheetID},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(data){
                    if(data.status == 'running'){
                        updateStatusAndId(data.status,data.id);
                    }
                    else{
                        updateStatusAndId(null,null);
                    }
                    loadTable();
                }
            })

        })


    })

</script>


<!-- To be Deleted!! -->
