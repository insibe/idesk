@extends('layouts.guest-dashboard')

@section('content')
    <div class="nk-content-wrap">
        <div class="row g-gs -align-center">

            <div class="col-6">
               <img src="{{ asset('backend/images/bg_img.png') }}">
            </div>
            <div class="col-6">
                <div class="nk-block">
                    <div class="nk-block-head">

                    </div><!-- .nk-block-head -->
                    {!! Form::model($category, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                    <div class="form-group">
                        <label class="form-label" >Name<span>*</span></label>
                        <div class="form-control-wrap">
                            {!! Form::text('name',null, ['class' => 'form-control', 'placeholder'=>'Please enter your name','required' =>'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" >Email<span>*</span></label>
                        <div class="form-control-wrap">
                            {!! Form::text('email',null, ['class' => 'form-control', 'placeholder'=>'Please enter your email','required' =>'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" >Mobile Number<span>*</span></label>
                        <div class="form-control-wrap">
                            {!! Form::text('mobileNumber',null, ['class' => 'form-control', 'placeholder'=>'Please enter your mobile number','required' =>'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" >Select Service <span>*</span></label>
                        <div class="form-control-wrap">
                            {!! Form::select('status',[''=>'','0'=>'Active','1'=>'Inactive'] ,null, ['data-parsley-errors-container' => '#status-errors','data-placeholder' => 'Select Category Status','class' => 'form-control form-select', 'data-search'=>'off', 'required' =>'required']) !!}
                            <div id="status-errors"></div>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="form-label" for="phone-no">Short Description</label>
                        <div class="form-control-wrap">
                            {!! Form::textarea('description',null, ['class' => 'form-control','rows' => 1,'placeholder'=>'Enter  Description..']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-lg btn-primary">Get Started Now!</button>
                    </div>
                    {!! Form::close() !!}

                </div><!-- .nk-block -->
            </div>
        </div>




    </div>

@endsection
