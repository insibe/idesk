<?php

use Illuminate\Support\Facades\Route;
use App\Models\Category;
use App\Models\Act;
use App\Models\Section;
use App\Models\company;
use App\Models\Project;
use App\Models\TimeSheet;
use Illuminate\Support\Facades\Artisan;
use Carbon\Carbon;
use Rinvex\Subscriptions\Models\PlanFeature;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = [
        'category' => null,
        'formMethod' => 'POST',
        'url' => 'dashboard/categories',
        'page_title' => 'Add New '
    ];
    return view('home',$data );
});

Route::get('category/{id}/acts/', function ($id) {

    $category = Category::where('id', $id )
        ->firstOrFail();

    $getacts = $category->acts()->orderBy('wef','desc')->get();


    return view('acts',compact('getacts'));
});



Auth::routes();



Route::get('/packages', function() {
    $packages= \Rinvex\Subscriptions\Models\Plan::all();


    return view('packages',compact('packages'));
});


Route::group(['middleware' => 'auth'], function () {

    Route::get('user/dashboard', function () {
        return view('user.dashboard');
    });
    // Admin Dashboard
    Route::get('dashboard', function () {
        return view('dashboard.index');
    });
    Route::get('/user/profile', function () {
        return view('user.profile');
    });
    Route::get('/user/profile/basic-info', function () {
        return view('user.basic-info');
    });

    Route::resource('dashboard/categories', \App\Http\Controllers\CategoryController::class);
    Route::resource('dashboard/services', \App\Http\Controllers\TenderController::class);
    Route::resource('dashboard/packages', \App\Http\Controllers\PackageController::class);
    Route::resource('dashboard/society', \App\Http\Controllers\SocietyController::class);
    Route::resource('dashboard/timesheet', \App\Http\Controllers\TimeSheetController::class);

});



Route::get('/fetchTimeSheet', [App\Http\Controllers\TimeSheetController::class, 'FetchData']);

Route::get('/loadTable', [App\Http\Controllers\TimeSheetController::class, 'LoadTable']);

Route::get('/updateTimer', [App\Http\Controllers\TimeSheetController::class, 'UpdateTimer']);

Route::get('/timerStartStop', [App\Http\Controllers\TimeSheetController::class, 'StartAndStopTimer']);


Route::get('/my-timesheet', function() {

    $date = empty($date) ? Carbon::now() : Carbon::createFromDate($date);
    $startOfCalendar = $date->copy()->startOfWeek(Carbon::SUNDAY);
    $endOfCalendar = $date->copy()->endOfWeek(Carbon::SATURDAY);


    $html = '<div class="col-lg-12"><div class="btn-group" aria-label="Basic example">  <button type="button" class="btn btn-outline-lighter">Previous Week </button><button type="button" class="btn btn-outline-lighter">Current Week</button>  <button type="button" class="btn btn-outline-lighter">Next Week</button></div> </div>';
    $html .= '<div class="col-lg-12">';
    $html .= '<span class="month">' . $date->format('M') . '</span>';
    $html .= '<span class="year">' . $date->format('Y') . '</span>';
    $html .= '</div>';
    $html .= '<div class="col-lg-12" style="background: #f8f8f8">';
    $html .= '<div class="days">';

    $dayLabels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    foreach ($dayLabels as $dayLabel)
    {
        $html .= '<span class="day-label">' . $dayLabel . '</span>';
    }

    while($startOfCalendar <= $endOfCalendar)
    {
        $extraClass = $startOfCalendar->format('m') != $date->format('m') ? 'dull' : '';
        $extraClass .= $startOfCalendar->isToday() ? ' today' : '';

        $html .= '<span class="day '.$extraClass.'"><span class="content">' . $startOfCalendar->format('j') . '</span></span>';
        $startOfCalendar->addDay();
    }
    $html .= '</div></div> </div>';

    $timeSheets = TimeSheet::where([
                    ['status','running'],
                    ['user_id',auth()->user()->id]
                ])
                ->get();

                // dd(date('H:i:s'));
    foreach($timeSheets as $timesheet){
        try{
            $startdate = new DateTime($timesheet->start_time);
            $interval = $startdate->diff(new DateTime(date('Y-m-d H:i:s')));
            $minutes = $interval->days * 24 * 60;
            $minutes += $interval->h * 60;
            $minutes += $interval->i;

            //update timesheet record
            $timesheet->start_time = date('Y-m-d H:i:s');
            $timesheet->total_hours = $timesheet->total_hours + $minutes;
            $timesheet->update();
        }
        catch (\Exception $e) {
            return $e;
        }
    }


    $modaldata = company::join('projects','projects.company_id','=','companies.id')
            ->select('projects.company_id','companies.company_name','projects.id','projects.project_name','projects.notes')
            ->where('user_id','=',auth()->user()->id)
            ->get();

    $data = [
        'timesheet' => null,
        'modaldata' => $modaldata,
        'formMethod' => 'POST',
        'url' => 'dashboard/timesheet',
        'page_title' => 'Manage Timesheet'
    ];

    return view('user.timesheet.index',compact('html'),$data);
});


Route::get('/getActsLaws', function () {
    $getActsLaws = Category::defaultOrder()->get();
    return $getActsLaws;
});

Route::get('/getSections', function () {
    $getSections = Section::with('subsections')->get();

    return view('home');

});


//Clear Cache facade value:
Route::get('/update', function() {
    $plan = app('rinvex.subscriptions.plan')->find(1);

    return $plan;
});



//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('optimize');
    $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    return '<h1>Cache facade value cleared</h1>';
});


Route::get('/migrate', function(){
    Artisan::call('migrate');
    return '<h1>MIGRATE</h1>';
});
