<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('locale');
            $table->string('tender_no');
            $table->string('slug');
            $table->longText('description');
            $table->string('value');
            $table->string('emd')->nullable();
            $table->string('document_fee')->nullable();
            $table->dateTime('last_date');
            $table->enum('sticky', ['0', '1']);
            $table->enum('status', ['0', '1']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
