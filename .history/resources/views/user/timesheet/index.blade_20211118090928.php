@extends('layouts.app')

@section('content')


    <style type="text/css">
        .calendar {
            display: flex;
            position: relative;
            padding: 16px;
            margin: 0 auto;
            max-width: 320px;
            background: white;
            border-radius: 4px;
            box-shadow: 0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04);
        }

        .timesheet-header{

        }

        .month-year {
            position: absolute;
            bottom:62px;
            right: -27px;
            font-size: 2rem;
            line-height: 1;
            font-weight: 300;
            color: #94A3B8;
            transform: rotate(90deg);
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
        }

        .year {
            margin-left: 4px;
            color: #CBD5E1;
        }

        /*.days {*/
        /*    display: flex;*/
        /*    flex-wrap: wrap;*/
        /*    flex-grow: 1;*/
        /*    margin-right: 46px;*/
        /*}*/

        /*.day-label {*/
        /*    position: relative;*/
        /*    flex-basis: calc(14.286% - 2px);*/
        /*    margin: 1px 1px 12px 1px;*/
        /*    font-weight: 700;*/
        /*    font-size: 0.65rem;*/
        /*    text-transform: uppercase;*/
        /*    color: #1E293B;*/
        /*}*/

        /*.day {*/
        /*    position: relative;*/
        /*    flex-basis: calc(14.286% - 2px);*/
        /*    margin: 1px;*/
        /*    cursor: pointer;*/
        /*    font-weight: 300;*/
        /*}*/

        /*.day.dull {*/
        /*    color: #94A3B8;*/
        /*}*/

        /*.day.today {*/
        /*    color: #0EA5E9;*/
        /*    font-weight: 600;*/
        /*}*/

        /*.day::before {*/
        /*    content: '';*/
        /*    display: block;*/
        /*    padding-top: 100%;*/
        /*}*/

        /*.day:hover {*/
        /*    background: #E0F2FE;*/
        /*}*/

        /*.day .content {*/
        /*    position: absolute;*/
        /*    top: 0;*/
        /*    left: 0;*/
        /*    height: 100%;*/
        /*    width: 100%;*/
        /*    display: flex;*/
        /*    justify-content: center;*/
        /*    align-items: center;*/
        /*}*/
    </style>
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">


            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title"</h3>
                <div class="nk-block-des text-soft">
                    <p>Wednesday, 27 October.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/timesheet/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Add Entry </span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="row g-gs">
            {!! $html !!}
            
            
            <!-- Styled Table -->
            <div class="card card-bordered card-preview">
                
                <div class = "btn-group">
                    <button type="button" class="btn btn-outline-light">Mon </button>
                    <button type="button" class="btn btn-outline-light">Tue </button>
                    <button type="button" class="btn btn-outline-light">Wed </button>
                    <button type="button" class="btn btn-outline-light">Thur </button>
                    <button type="button" class="btn btn-outline-light">Fri </button>
                    <button type="button" class="btn btn-outline-light">Sat </button>
                    <button type="button" class="btn btn-outline-light">Sun </button>
                </div>
                
                <table class = "table" id = "table1">
                
                </table>
            </div>

            <!-- Styled Table -->
            

            <!-- Modal Form -->
            <div class="modal fade" tabindex="-1" id="modalForm">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">New entry for </h5>
                            <a href="#" class="close" data-dismiss="modal" aria-label="Close">
                                <em class="icon ni ni-cross"></em>
                            </a>
                        </div>
                        <div class="modal-body">
                            {!! Form::model($timesheet, array( 'method' => $formMethod, 'data-parsley-validate', 'url' => $url ,'class'=>'horizontal-form' ,'files' => 'true', 'enctype'=>'multipart/form-data')) !!}
                            <!-- <form action="#" class="form-validate is-alter"> -->
                                <div class="form-group">
                                    <label class="form-label">Projects / Tasks</label>
                                    <div class="form-control-wrap">
                                    {!! Form::select('projectName',null, ['class' => 'form-select', 'placeholder'=>'Package Name','required' =>'required']) !!}
                                        <!-- <select class="form-select" data-search="on" id = "projectName"> -->
                                            <!-- <option value="Project 1">Project 1</option>
                                            <option value="Project 2">Project 2</option>
                                            <option value="Project 3">Project 3</option> -->
                                        </select>
                                    </div>
                                </div>
                                <!-- <div class="form-group">
                                    <label class="form-label" for="projectName">Projects / Tasks</label>
                                    <div class="form-control-wrap ">
                                        <div class="form-control-select">
                                            <select class="form-control" id="projectName">
                                                <option value="default_option">Example Project 1</option>
                                                <option value="option_select_name">Example Project 2</option>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <label class="form-label">Category</label>
                                    <div class="form-control-wrap">
                                        <!-- <select class="form-select" data-search="on" id = "category"> -->
                                        {!! Form::select('category',null, ['class' => 'form-select', 'placeholder'=>'Package Name','required' =>'required']) !!}
                                            <option value="Programming">Programming</option>
                                            <option value="Marketing">Marketing</option>
                                            <option value="Testing">Testing</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <!-- <div class="form-group">
                                    <label class="form-label" for="category">Category</label>
                                    <div class="form-control-wrap">
                                        <input type="text" class="form-control" id="category" required>
                                    </div>
                                </div> -->
                                <div class = "row">

                                    <div class="col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control-wrap">
                                                <div class="form-icon form-icon-right xl">
                                                    <em class="icon ni ni-calendar-alt"></em>
                                                </div>
                                                {!! Form::text('date',null, ['class' => 'form-select', 'placeholder'=>'Package Name','required' =>'required']) !!}
                                                <!-- <input type="text" class="form-control form-control-xl form-control-outlined date-picker" id="date"> -->
                                                <label class="form-label-outlined" for="date">Date (DD/MM/YYYY)</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-sm-6">
                                        <div class="form-group">
                                            <div class="form-control-wrap">
                                                <div class="form-icon form-icon-right xl">
                                                    <em class="icon ni ni-clock"></em>
                                                </div>
                                                <!-- <input type="text" class="form-control form-control-xl form-control-outlined" id="duration"> -->
                                                {!! Form::text('duration',null, ['class' => 'form-select', 'placeholder'=>'Package Name','required' =>'required']) !!}
                                                <label class="form-label-outlined" for="duration">Duration</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label class="form-label" for="comments">Comments</label>
                                    <div class="form-control-wrap">
                                        <!-- <textarea class="form-control no-resize" id="comments" placeholder = "Notes (optional)"></textarea> -->
                                        {!! Form::textarea('comments',null, ['class' => 'form-select', 'placeholder'=>'Package Name','required' =>'required']) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="button" class="btn btn-lg btn-primary" data-dissmiss="modal" id = "btn1" value = "Start Timer">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Modal -->

            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalForm">Modal With Form</button>
        </div>
    </div>


@endsection


<!-- To be Deleted!! -->
<!-- Ajax / JQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>



<script type="text/javascript">
    $(document).ready(function(){
        $("#btn1").click(function(){
            var projectName = $("#projectName").val()
            var category = $("#category").val()
            var notes = $("#comments").val()
            var date = $("#date").val()
            var duration = $("#duration").val();
            var htmlQuery = "<tr class = 'tb-tnx-item'><td> <ul class = 'list-group'><li class = 'list-group-item border-0 py-0.3'><h5>" + projectName + " </h5><li>" ;
            htmlQuery += "<li class = 'list-group-item border-0 py-0'> " + category + " </li>" ;
            htmlQuery += "<li class = 'list-group-item border-0 py-0'> " + notes + " </li></ul>";
            htmlQuery += "<td width='10%'><h4> 0:00 </h4></div></td>"
            htmlQuery += "<td width='10%'><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalForm'>Start</button></td>"
            htmlQuery += "<td width='10%'><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalForm'>Edit</button></td></tr>"
            $("#table1").append(htmlQuery)
            $("#modalForm").modal('hide')
        })
    })
</script>


<!-- To be Deleted!! -->