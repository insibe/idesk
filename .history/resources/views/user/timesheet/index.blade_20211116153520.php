@extends('layouts.app')

@section('content')


    <style type="text/css">
        .calendar {
            display: flex;
            position: relative;
            padding: 16px;
            margin: 0 auto;
            max-width: 320px;
            background: white;
            border-radius: 4px;
            box-shadow: 0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04);
        }

        .timesheet-header{

        }

        .month-year {
            position: absolute;
            bottom:62px;
            right: -27px;
            font-size: 2rem;
            line-height: 1;
            font-weight: 300;
            color: #94A3B8;
            transform: rotate(90deg);
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
        }

        .year {
            margin-left: 4px;
            color: #CBD5E1;
        }

        /*.days {*/
        /*    display: flex;*/
        /*    flex-wrap: wrap;*/
        /*    flex-grow: 1;*/
        /*    margin-right: 46px;*/
        /*}*/

        /*.day-label {*/
        /*    position: relative;*/
        /*    flex-basis: calc(14.286% - 2px);*/
        /*    margin: 1px 1px 12px 1px;*/
        /*    font-weight: 700;*/
        /*    font-size: 0.65rem;*/
        /*    text-transform: uppercase;*/
        /*    color: #1E293B;*/
        /*}*/

        /*.day {*/
        /*    position: relative;*/
        /*    flex-basis: calc(14.286% - 2px);*/
        /*    margin: 1px;*/
        /*    cursor: pointer;*/
        /*    font-weight: 300;*/
        /*}*/

        /*.day.dull {*/
        /*    color: #94A3B8;*/
        /*}*/

        /*.day.today {*/
        /*    color: #0EA5E9;*/
        /*    font-weight: 600;*/
        /*}*/

        /*.day::before {*/
        /*    content: '';*/
        /*    display: block;*/
        /*    padding-top: 100%;*/
        /*}*/

        /*.day:hover {*/
        /*    background: #E0F2FE;*/
        /*}*/

        /*.day .content {*/
        /*    position: absolute;*/
        /*    top: 0;*/
        /*    left: 0;*/
        /*    height: 100%;*/
        /*    width: 100%;*/
        /*    display: flex;*/
        /*    justify-content: center;*/
        /*    align-items: center;*/
        /*}*/
    </style>
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">


            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title"</h3>
                <div class="nk-block-des text-soft">
                    <p>Friday, 27 October.</p>
                </div>
            </div><!-- .nk-block-head-content -->
            <div class="nk-block-head-content">
                <div class="toggle-wrap nk-block-tools-toggle">
                    <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="more-options"><em class="icon ni ni-more-v"></em></a>
                    <div class="toggle-expand-content" data-content="more-options">
                        <ul class="nk-block-tools g-3">

                            <li class="nk-block-tools-opt">
                                <a href="#" class="btn btn-icon btn-primary d-md-none"><em class="icon ni ni-plus"></em></a>
                                <a href="{{ url('/dashboard/timesheet/create') }}" class="btn btn-primary d-none d-md-inline-flex"><em class="icon ni ni-plus"></em><span>Add Entry </span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- .nk-block-head-content -->
        </div><!-- .nk-block-between -->
    </div><!-- .nk-block-head -->
    <div class="nk-block">
        <div class="row g-gs">
            {!! $html !!}
        </div>

    </div>

@endsection
