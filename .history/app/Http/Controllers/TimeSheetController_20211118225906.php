<?php

namespace App\Http\Controllers;

use App\Models\TimeSheet;
use Illuminate\Http\Request;

class TimeSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        //
        try{
            $timeSheet = new TimeSheet();
            $timeSheet -> save();

            $htmlQuery = "<tr class = 'tb-tnx-item'><td> <ul class = 'list-group'><li class = 'list-group-item border-0 py-0.3'><h5>" . $request->projectName . "</h5><li>" ;
            $htmlQuery .= "<li class = 'list-group-item border-0 py-0'> " . $request->category . " </li>" ;
            $htmlQuery .= "<li class = 'list-group-item border-0 py-0'> " . $request->comments . " </li></ul>";
            $htmlQuery .= "<td width='10%'><h4> 0:00 </h4></div></td>";
            $htmlQuery .= "<td width='10%'><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalForm'>Start</button></td>";
            $htmlQuery .= "<td width='10%'><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#modalForm'>Edit</button></td></tr>";
            
            return $htmlQuery;
        }
        catch (\Exception $e) {
            return $e;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function show(TimeSheet $timeSheet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeSheet $timeSheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TimeSheet $timeSheet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeSheet $timeSheet)
    {
        //
    }
}
