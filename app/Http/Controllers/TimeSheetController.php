<?php

namespace App\Http\Controllers;

use App\Models\TimeSheet;
use App\Models\company;
use App\Models\Project;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TimeSheetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        //
        try{
            $timeSheet = new TimeSheet();
            $timeSheet->date = $request->date;
            $timeSheet->user_id = auth()->user()->id;
            $timeSheet->company_id = 1;
            $timeSheet->category = $request->category;
            $timeSheet->project_id = $request->projectID;
            $timeSheet->status = 'running';
            $timeSheet->start_time = date('Y-m-d H:i:s');
            $timeSheet->total_hours = $request->time;
            $timeSheet->comments = $request->comments;
            $timeSheet ->save();

        }
        catch (\Exception $e) {
            return $e;
        }
    }

    public function FetchData(Request $request)
    {
        $timesheetdata = TimeSheet::join('projects','projects.id','=','time_sheets.project_id')
                        ->where('time_sheets.id','=',$request->id)
                        ->select('time_sheets.*','projects.project_name')
                        ->get();
        return $timesheetdata;
    }

    public function LoadTable(Request $request)
    {
        $timesheetdata = TimeSheet::join('projects','projects.id','=','time_sheets.project_id')
                        ->select('time_sheets.*','projects.project_name')
                        ->get();
        

        $htmlQuery = '';
        $status = null;
        $id = null;
        foreach($timesheetdata as $ts)
        {
            if($ts->status == 'running'){
                $status = 'running';
                $id = $ts->id;
            }

            $hours = floor($ts->total_hours / 60);
            $minutes = $ts->total_hours % 60; 

            $time = sprintf("%02d:%02d", $hours, $minutes);

            $htmlQuery .= "<tr class = 'tb-tnx-item' id = " . $ts->id . ">
                                <td> 
                                <ul class = 'list-group'>
                                    <li class = 'list-group-item border-0 py-0.3'><h5>" . $ts->project_name . "</h5><li> 
                                    <li class = 'list-group-item border-0 py-0'> " . $ts->category . " </li>
                                    <li class = 'list-group-item border-0 py-0'> " . $ts->comments . " </li>
                                </ul>
                                <td width='10%'>
                                    <h4> " . $time . " </h4>
                                </td>";
            if($ts->status == 'running'){
                $htmlQuery .= "<td width='10%'>
                                    <button type='button' class='btn btn-danger startstopBtn'>Stop</button>
                                </td>";
            }
            else{
                $htmlQuery .= "<td width='10%'>
                                    <button type='button' class='btn btn-primary startstopBtn'>Start</button>
                                </td>";
            }
            $htmlQuery .="<td width='10%'>
                                <button type='button' class='btn btn-primary editBtn' data-toggle='modal' data-target='#modalForm'>Edit</button>
                            </td>
                        </tr>";
        }
        $data = [
            'status' => $status,
            'id' => $id,
            'htmlQuery' => $htmlQuery
        ];
        return $data;
    }

    public function UpdateTimer(Request $request)
    {
        $ts = TimeSheet::where('id',$request->id)->firstOrFail();
        $ts->start_time = date('Y-m-d H:i:s');
        $ts->total_hours = $ts->total_hours + 1;
        $ts->update();
        return 'success';
    }

    public function StartAndStopTimer(Request $request)
    {
        $status = null;
        $timesheet = TimeSheet::where('id',$request->id)->firstOrFail();
        if($timesheet->status == 'running'){
            $timesheet->status = 'stopped';
            $timesheet->end_time = date('Y-m-d H:i:s');
            $timesheet->update();
            $status = null;
        }
        else{
            $timesheet->status = 'running';
            $timesheet->start_time = date('Y-m-d H:i:s');
            $timesheet->end_time = null;
            $timesheet->update();
            $status = 'running';
        }
        $data = [
            'status' => $status,
            'id' => $request->id
        ];
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function show(TimeSheet $timeSheet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function edit(TimeSheet $timeSheet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TimeSheet $timeSheet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TimeSheet  $timeSheet
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimeSheet $timeSheet)
    {
        //
    }
}
