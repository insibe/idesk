<?php

namespace App\Http\Controllers;

use App\Models\Package;
use Illuminate\Http\Request;
use Rinvex\Subscriptions\Models\Plan;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $packages =  Plan::paginate(15);

        $data = [
            'page_title' => 'Manage Packages'
        ];

        return view('dashboard.packages.index',compact('packages'),$data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'package' => null,
            'formMethod' => 'POST',
            'url' => 'dashboard/packages',
            'page_title' => 'Add New Package '
        ];

        return view('dashboard.packages.edit',$data );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plan = app('rinvex.subscriptions.plan')->create([
            'name' => $request->get('name') ,
            'description' => $request->get('description') ,
            'price' => $request->get('price') ,
            'signup_fee' => $request->get('signup_fee') ,
            'invoice_period' => $request->get('invoice_period') ,
            'invoice_interval' => $request->get('invoice_interval') ,
            'trial_period' => $request->get('trial_period') ,
            'trial_interval' => $request->get('trial_interval') ,
            'sort_order' => 1,
            'currency' => $request->get('currency') ,
            'is_active' => $request->get('status')
        ]);

        return $plan;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Package  $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        //
    }
}
